//
//  ViewController.swift
//  TechnicalExam
//
//  Created by Michael Dean Villanda on 21/02/2020.
//  Copyright © 2020 Michael Dean Villanda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    lazy var checkBox: UICheckBox = {
        let checkBox = UICheckBox(state: .normal)
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(checkBox)
        
        NSLayoutConstraint.activate([
            checkBox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 100.0),
            checkBox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24.0),
            checkBox.heightAnchor.constraint(equalToConstant: 32.0),
            checkBox.widthAnchor.constraint(equalToConstant: 32.0)
            ])
        
        return checkBox
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        self.view.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: self.checkBox.trailingAnchor, constant: 8.0),
            label.topAnchor.constraint(equalTo: self.checkBox.topAnchor, constant: 0)
            ])
        
        return label
    }()
    
    lazy var showUsageButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(button)
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: self.checkBox.bottomAnchor, constant: 32.0),
            button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24.0),
            button.heightAnchor.constraint(equalToConstant: 50.0),
            button.widthAnchor.constraint(equalToConstant: 180.0)
            ])
        
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        checkBox.delegate = self
        checkBox.checkBoxState = .normal
        checkBox.backgroundColor = .lightGray
        
        titleLabel.text = "NORMAL"
        
        showUsageButton.setTitle("SHOW USAGE", for: .normal)
        showUsageButton.backgroundColor = .red
        showUsageButton.addTarget(self, action: #selector(showUsage), for: .touchUpInside)
    }

    @objc func showUsage() {
        let viewController = UsageViewController()
        
        let nav = UINavigationController(rootViewController: viewController)
        self.present(nav, animated: true, completion: nil)
    }
}

extension ViewController: UICheckBoxDelegate {
    func didTapCheckbox(_ checkbox: UICheckBox) {
        switch checkbox.checkBoxState {
        case .checked:
            titleLabel.text = "CHECKED"
        case .indeterminate:
            titleLabel.text = "indeterminate".uppercased()
        default:
            titleLabel.text = "NORMAL"
        }
    }
}
