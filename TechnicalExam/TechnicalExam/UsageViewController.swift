//
//  UsageViewController.swift
//  TechnicalExam
//
//  Created by Michael Dean Villanda on 21/02/2020.
//  Copyright © 2020 Michael Dean Villanda. All rights reserved.
//

import UIKit

class UsageViewController: UIViewController {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
            ])
        
        return tableView
    }()
    
    var itemArray: [Item] = []
    
    var refArray: [Item] = Item.sampleData
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(willCloseView))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        
        tableView.register(ChildTableViewCell.self, forCellReuseIdentifier: childCellIdentifier)
        tableView.register(ParentTableViewCell.self, forHeaderFooterViewReuseIdentifier: parentCellIdentifier)
        
        if let first = refArray.first {
            itemArray = [first]
        }
        
        tableView.reloadData()
    }
    
    @objc func willCloseView() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UsageViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: childCellIdentifier) as? ChildTableViewCell else {
            return UITableViewCell()
        }
        
        let child = itemArray[indexPath.row]
        
        cell.configureCell(child, forIndexPath: indexPath, inTableView: self.tableView)
        cell.checkBox.delegate = self
        
        return cell
    }
}

extension UsageViewController: UICheckBoxDelegate {
    func didTapCheckbox(_ checkbox: UICheckBox) {
        guard let item = checkbox.item else { return }
        
        shouldAppendChild(checkbox.indexRow, item: item)
        
        tableView.reloadData()
    }
}

extension UsageViewController {
    func shouldAppendChild(_ index: Int, item: Item) {
        if item.state == .checked || item.state == .indeterminate {
            if item.isLeaf {
                item.state = .normal
            } else {
                if item.isRootParent {
                    if let first = refArray.first {
                        itemArray = [first]
                    }
                } else {
                    itemArray.removeLast()
                }
            }
            
            itemArray.forEach { (node) in
                if node.isLeaf {
                    return
                }
                
                if itemArray.count == 1 {
                    node.state = .normal
                } else {
                    if item.isLeaf {
                        node.state = .indeterminate
                    } else {
                        node.state = (!node.isRootParent) ? .normal: .indeterminate
                    }
                }
            }
            
        } else {
            if item.isLeaf {
                item.state = .checked
                
                let tempArr = itemArray.filter({ $0.id != item.id })
                
                tempArr.forEach { (node) in
                    node.state = item.state
                }
                
            } else {
                item.state = .indeterminate
                guard let new = refArray.first(where: { $0.id == (index + 1) }) else { return }
                new.state = .normal
                
                itemArray.append(new)
            }
        }
    }
}
