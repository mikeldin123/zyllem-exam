//
//  Item.swift
//  TechnicalExam
//
//  Created by Michael Dean Villanda on 21/02/2020.
//  Copyright © 2020 Michael Dean Villanda. All rights reserved.
//

import Foundation


public class Item {
    var id: Int
    var title: String
    var state: CheckBoxState
    
    var parent: Item?
    
    var child: [Item] = []
    
    init(id: Int, title: String, state: CheckBoxState) {
        self.id = id
        self.title = title
        self.state = state
    }
}

extension Item {
    var isRootParent: Bool {
        return parent == nil
    }
    
    var isLeaf: Bool {
        return child.count == 0
    }
    
    var isParent: Bool {
        return child.count != 0
    }
    
    var isChild: Bool {
        return parent != nil
    }
    
    var allChildSelected: Bool {
        return numberOfChild == numberOfOpenChild
    }
    
    var numberOfChild: Int {
        return child.count
    }
    
    var numberOfOpenChild: Int {
        return child.filter({ $0.state == .checked }).count
    }

    func getLevel() -> Int {
        return parent == nil ? 0: (self.parent?.getLevel() ?? 0) + 1
    }
}

extension Item {

}

extension Item {
    static var sampleData: [Item] {
        let level1 = Item(id: 0, title: "Level 1", state: .normal)
        
        let level2 = Item(id: 1, title: "Level 2", state: .normal)
        
        let level3 = Item(id: 2, title: "Level 3", state: .normal)
        level3.parent = level2
        
        level2.parent = level1
        level2.child = [level3]
        
        level1.child = [level2, level3]
        
        return [level1, level2, level3]
    }
}
