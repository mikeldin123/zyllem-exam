//
//  ParentTableViewCell.swift
//  TechnicalExam
//
//  Created by Michael Dean Villanda on 21/02/2020.
//  Copyright © 2020 Michael Dean Villanda. All rights reserved.
//

import UIKit

let parentCellIdentifier = "parent-cell-identifier"

class ParentTableViewCell: UITableViewHeaderFooterView {
    lazy var checkBox: UICheckBox = {
        let checkBox = UICheckBox(state: .normal)
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(checkBox)
        
        NSLayoutConstraint.activate([
            checkBox.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16.0),
            checkBox.widthAnchor.constraint(equalToConstant: 32.0),
            checkBox.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 8.0),
            checkBox.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
            ])
        
        return checkBox
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        self.contentView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: self.checkBox.trailingAnchor, constant: 8.0),
            label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            label.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor)
            ])
        
        return label
    }()
    
    func configureCell(_ item: Item, forSection section: Int, inTableView tableView: UITableView) {
        
        checkBox.item = item
        checkBox.checkBoxState = item.state
        titleLabel.text = item.title
    }
}
