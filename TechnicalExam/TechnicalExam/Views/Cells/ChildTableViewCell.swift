//
//  ChildTableViewCell.swift
//  TechnicalExam
//
//  Created by Michael Dean Villanda on 21/02/2020.
//  Copyright © 2020 Michael Dean Villanda. All rights reserved.
//

import UIKit

let childCellIdentifier = "child-cell-identifier"

class ChildTableViewCell: UITableViewCell {
    
    var leftPadding: NSLayoutConstraint?
    
    lazy var checkBox: UICheckBox = {
        let checkBox = UICheckBox(state: .normal)
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(checkBox)
        
        leftPadding = checkBox.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16.0)
        
        NSLayoutConstraint.activate([
            leftPadding!,
            checkBox.widthAnchor.constraint(equalToConstant: 32.0),
            checkBox.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 8.0),
            checkBox.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
            ])
        
        return checkBox
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        self.contentView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: self.checkBox.trailingAnchor, constant: 8.0),
            label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            label.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor)
            ])
        
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func configureCell(_ item: Item, forIndexPath indexPath: IndexPath, inTableView tableView: UITableView) {
        checkBox.item = item
        checkBox.indexRow = indexPath.row
        checkBox.checkBoxState = item.state
        titleLabel.text = item.title
        
        if item.isRootParent {
            leftPadding?.constant = 16.0
        } else {
            leftPadding?.constant = CGFloat((item.getLevel() > 1) ? 32.0: 16.0) + 16.0
        }
       
    }
}
