//
//  UICheckBox.swift
//  TechnicalExam
//
//  Created by Michael Dean Villanda on 21/02/2020.
//  Copyright © 2020 Michael Dean Villanda. All rights reserved.
//

import UIKit

enum CheckBoxState {
    case normal
    case indeterminate
    case checked
    
    var icon: UIImage {
        switch self {
        case .checked:
            return #imageLiteral(resourceName: "ticked-checkbox").withRenderingMode(.alwaysTemplate)
        case .indeterminate:
            return #imageLiteral(resourceName: "indeterminate-checkbox").withRenderingMode(.alwaysTemplate)
        default:
            return #imageLiteral(resourceName: "outlined-checkbox").withRenderingMode(.alwaysTemplate)
        }
    }
    
    var tint: UIColor {
        switch self {
        case .checked:
            return .green
        case .indeterminate:
            return .yellow
        default:
            return .black
        }
    }
}

protocol UICheckBoxDelegate: class {
    func didTapCheckbox(_ checkbox: UICheckBox)
}

class UICheckBox: UIButton {
    lazy var checkBox: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: self.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
        
        return imageView
    }()
    
    weak var delegate: UICheckBoxDelegate?
    
    var item: Item?

    var indexRow: Int = 0
    
    var checkBoxState: CheckBoxState = .normal {
        didSet {
            checkBox.image = checkBoxState.icon
            checkBox.tintColor = checkBoxState.tint
        }
    }

    convenience init(state: CheckBoxState) {
        self.init()
        
        checkBoxState = state
  
        self.addTarget(self, action: #selector(didPress), for: .touchUpInside)
    }
    
    @objc func didPress() {
        if item != nil {
            delegate?.didTapCheckbox(self)
        } else {
            switch checkBoxState {
            case .checked:
                checkBoxState = .normal
            case .indeterminate:
                checkBoxState = .checked
            case .normal:
                checkBoxState = .indeterminate
            }
            
            delegate?.didTapCheckbox(self)
        }
    }
}
